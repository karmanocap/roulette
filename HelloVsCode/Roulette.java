package HelloVsCode;

public class Roulette {
    public static void main(String[] args) {
        java.util.Scanner ask = new java.util.Scanner(System.in);

        int userWinnings = 0;
        int game = 1;
        RouletteWheel spinner = new RouletteWheel();

        while (game == 1) {
            System.out.println("Would you like to bet? (y or n)");
            String answerOne = ask.next();
            if (answerOne.equals("y")) {
                System.out.println("Please enter your amount for the bet.");
                    int betAmount = ask.nextInt();
                System.out.println("Choose a number you will be betting on. (0-36)");
                    int betNumber = ask.nextInt();
                spinner.spin();
                int winnerNumber = spinner.getValue();
                if (winnerNumber == betNumber) {
                    userWinnings = userWinnings + (betAmount * 35);
                }
                else {
                    userWinnings = userWinnings - betAmount;
                }
            }
            else {
                System.out.println("Thanks for playing! Your winnings/losses are :"+ userWinnings +"$.");
                game = 0;
            }
        }
    }
}